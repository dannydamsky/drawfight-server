The server for the DrawFight game. All rights reserved to John Bryce students of the Android Developer course (33rd period guided by Tsur Ben Ami)


[View the Android client repository](https://bitbucket.org/dannydamsky/drawfight-android)


[Download the game dev plan (Written in Hebrew)](https://bitbucket.org/dannydamsky/drawfight-android/downloads/DrawFight.docx)